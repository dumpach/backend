const {
  Board, Thread, Post, Attachment, Reply,
} = require('../../db').models;
const { db } = require('../../db');
const { mediaFiles } = require('../../modules');

class Repository {
  constructor(boardId) {
    this.boardId = boardId;
    this.boardModel = Board;
    this.model = Thread[boardId];
    this.postModel = Post[boardId];
    this.attachmentModel = Attachment[boardId];
    this.replyModel = Reply[boardId];
  }

  findBoard() {
    const { boardId } = this;

    return this.boardModel.findOne({
      where: {
        id: boardId,
      },
    });
  }

  findThreads(offset = 0) {
    const {
      boardId, model, postModel, attachmentModel, replyModel,
    } = this;

    return model.findAll({
      where: {
        board_id: boardId,
      },
      limit: 10,
      offset: parseInt(offset, 10),
      order: [
        ['updated_at', 'desc'],
        [{ model: postModel, as: 'posts' }, 'created_at', 'asc'],
        [
          { model: postModel, as: 'posts' },
          { model: attachmentModel, as: 'attachments' },
          'id',
          'asc',
        ],
        [
          { model: postModel, as: 'posts' },
          { model: replyModel, as: 'replies' },
          'reply_id',
          'asc',
        ],
      ],
      include: [
        {
          as: 'posts',
          model: postModel,
          include: [
            {
              as: 'attachments',
              model: attachmentModel,
            },
            {
              as: 'replies',
              model: replyModel,
              attributes: ['reply_id', 'reply_thread_id'],
            },
            {
              as: 'replies_on',
              model: replyModel,
              attributes: ['post_id', 'post_thread_id'],
            },
          ],
        },
      ],
    });
  }

  countThreads() {
    const { boardId, model } = this;

    return model.count({
      where: {
        board_id: boardId,
      },
    });
  }

  findThread(threadId) {
    const {
      boardId, model, postModel, attachmentModel, replyModel,
    } = this;

    return model.findOne({
      where: {
        board_id: boardId,
        id: threadId,
      },
      order: [
        ['updated_at', 'desc'],
        [{ model: postModel, as: 'posts' }, 'created_at', 'asc'],
        [
          { model: postModel, as: 'posts' },
          { model: attachmentModel, as: 'attachments' },
          'id',
          'asc',
        ],
        [
          { model: postModel, as: 'posts' },
          { model: replyModel, as: 'replies' },
          'reply_id',
          'asc',
        ],
      ],
      include: [
        {
          as: 'posts',
          model: postModel,
          include: [
            {
              as: 'attachments',
              model: attachmentModel,
            },
            {
              as: 'replies',
              model: replyModel,
              attributes: ['reply_id', 'reply_thread_id'],
            },
            {
              as: 'replies_on',
              model: replyModel,
              attributes: ['post_id', 'post_thread_id'],
            },
          ],
        },
      ],
    });
  }

  async createThread(fields, files, threadsLimit) {
    const {
      boardId, model, postModel, attachmentModel, replyModel,
    } = this;

    const threads = await model.findAll({
      where: {
        board_id: boardId,
      },
      order: [['updated_at', 'desc']],
    });

    return db.transaction(async (t) => {
      const thread = await model.create({ board_id: boardId }, { transaction: t });

      const post = await postModel.create(
        { ...fields, thread_id: thread.id },
        { transaction: t },
      );

      await Promise.all(
        fields.replies_on.map(async (postId) => {
          const origPost = await postModel.findByPk(postId);
          return replyModel.create(
            {
              post_id: postId,
              reply_id: post.id,
              post_thread_id: origPost.thread_id,
              reply_thread_id: thread.id,
            },
            { transaction: t },
          );
        }),
      );

      // TODO: think how to separate work with db and files moving
      const attachmentsFields = await mediaFiles.moveFiles(
        files,
        boardId,
        thread.id,
      );

      const attachments = await Promise.all(
        attachmentsFields.map((attachment) =>
          attachmentModel.create(
            {
              ...attachment,
              post_id: post.id,
            },
            { transaction: t },
          )),
      );

      if (threads.length > threadsLimit - 1) {
        const threadsForDelete = threads.slice(threadsLimit - 1);

        await Promise.all(
          threadsForDelete.map((threadForDelete) =>
            Promise.all([
              threadForDelete.destroy({ transaction: t }),
              mediaFiles.removeThreadFiles(boardId, threadForDelete.id),
            ])),
        );
      }

      return [thread.toJSON(), post.toJSON(), attachments];
    });
  }

  updateThread(fields, files, thread) {
    const {
      boardId, postModel, attachmentModel, replyModel,
    } = this;

    return db.transaction(async (t) => {
      const post = await postModel.create(
        { ...fields, thread_id: thread.id },
        { transaction: t },
      );

      await Promise.all(
        fields.replies_on.map(async (postId) => {
          const origPost = await postModel.findByPk(postId);
          return replyModel.create(
            {
              post_id: postId,
              reply_id: post.id,
              post_thread_id: origPost.thread_id,
              reply_thread_id: thread.id,
            },
            { transaction: t },
          );
        }),
      );

      // TODO: think how to separate work with db and files moving
      const attachmentsFields = await mediaFiles.moveFiles(
        files,
        boardId,
        thread.id,
      );

      await Promise.all(
        attachmentsFields.map((attachment) =>
          attachmentModel.create(
            {
              ...attachment,
              post_id: post.id,
            },
            { transaction: t },
          )),
      );

      if (!post.is_sage) {
        thread.changed('updated_at', true);
        await thread.save({ transaction: t });
      }
    });
  }
}

module.exports = Repository;
