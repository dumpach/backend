const Router = require('koa-router');
const controller = require('./controller');

const router = new Router({
  prefix: '/api/v1/boards/:boardId/threads',
});

router.get('/', controller.list);
router.post('/', controller.create);
router.get('/:threadId', controller.get);
router.post('/:threadId', controller.update);

module.exports = router.routes();
