const { route: boardsApiRoutes } = require('./boards');
const { route: postsApiRoutes } = require('./posts');
const { route: threadsApiRoutes } = require('./threads');

module.exports = {
  routes: [boardsApiRoutes, postsApiRoutes, threadsApiRoutes],
};
