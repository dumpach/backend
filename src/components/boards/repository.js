const { Section, Board } = require('../../db').models;

const findBoards = () => Section.findAll({
  include: [Board],
});

const findBoard = (boardId) => Board.findOne({
  where: { id: boardId },
});

const repository = {
  findBoard,
  findBoards,
};

module.exports = repository;
