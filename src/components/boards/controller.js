const { HttpNotFoundException } = require('../../modules').errors;
const repository = require('./repository');

const list = async (ctx) => {
  try {
    const sections = await repository.findBoards();

    ctx.body = { data: sections };
  } catch (err) {
    throw err;
  }
};

const get = async (ctx) => {
  const { boardId } = ctx.params;

  try {
    const board = await repository.findBoard(boardId);

    if (!board) {
      throw new HttpNotFoundException('Board not found');
    }

    ctx.body = { data: board };
  } catch (err) {
    throw err;
  }
};

const controller = {
  list,
  get,
};

module.exports = controller;
