const {
  Post,
} = require('../../db').models;

class Repository {
  constructor(boardId, threadId) {
    this.threadId = threadId;
    this.model = Post[boardId];
  }

  findPost(postId) {
    const { threadId } = this;

    return this.model.findOne({
      where: {
        id: postId,
        thread_id: threadId,
      },
    });
  }
}

module.exports = Repository;
