const Router = require('koa-router');
const controller = require('./controller');

const router = new Router({
  prefix: '/api/v1/boards/:boardId/threads/:threadId/posts',
});

router.get('/:postId', controller.get);

module.exports = router.routes();
