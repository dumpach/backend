const { HttpNotFoundException } = require('../../modules').errors;
const Repository = require('./repository');

const get = async (ctx) => {
  const { boardId, threadId, postId } = ctx.params;

  try {
    const repository = new Repository(boardId, threadId);
    const post = await repository.findPost(postId);

    if (!post) {
      throw new HttpNotFoundException('Post not found');
    }

    ctx.body = { data: post };
  } catch (err) {
    throw err;
  }
};

const controller = {
  get,
};

module.exports = controller;
