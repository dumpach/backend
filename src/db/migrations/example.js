// Naming template: {fullYear}-{fullMonth}-{fullDay}-{migrationIndex}-{migrationName}
// Naming example:  2019-05-16-1-create-table-migrations

const Sequelize = require('sequelize');

const db = require('../connection');

module.exports = {
  up: () =>
    db.getQueryInterface().createTable('example', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('NOW()'),
      },
    }),
  down: () =>
    db.getQueryInterface().dropTable('example'),
};
