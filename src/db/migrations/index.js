const path = require('path');
const fs = require('fs-extra');

const { Migration } = require('../models');
const { logger } = require('../../modules');

const getMigrations = () => Migration.findAll();
const getDirFiles = () => fs.readdir(__dirname);
const getMigrationsNames = (files) =>
  files
    .map((file) => path.basename(file, '.js'))
    .filter((fileName) => fileName !== 'index' && fileName !== 'example');

const performMigration = async (migration, name) => {
  try {
    await migration.up();
    logger.info(`database - migration ${name} - success`);
    await Migration.create({ name });
  } catch (err) {
    logger.error(`database - migration ${name} - failure`);
    await migration.down();
    throw err;
  }
};

const performMigrations = (migrationsNames) => {
  const migrations = migrationsNames.reduce((acc, migrationName) => {
    /* eslint-disable */
    acc[migrationName] = require(`./${migrationName}.js`);
    /* eslint-enable */

    return acc;
  }, {});

  return Promise.all(
    Object.keys(migrations).map((key) => performMigration(migrations[key], key)),
  );
};

const init = async () => {
  try {
    const migrations = await getMigrations();
    const dirFiles = await getDirFiles();
    const migrationsNames = getMigrationsNames(dirFiles);
    const performedMigrationsNames = migrations.map((migration) => migration.name);
    const unperformedMigrationsNames = migrationsNames.reduce(
      (acc, migrationName) => {
        if (!performedMigrationsNames.includes(migrationName)) {
          acc.push(migrationName);
        }

        return acc;
      },
      [],
    );

    await performMigrations(unperformedMigrationsNames);
  } catch (err) {
    throw err;
  }
};

module.exports = { init };
