const Attachment = require('./Attachment');
const Board = require('./Board');
const Migration = require('./Migration');
const Post = require('./Post');
const Reply = require('./Reply');
const Section = require('./Section');
const Thread = require('./Thread');

module.exports = {
  Attachment,
  Board,
  Migration,
  Post,
  Reply,
  Section,
  Thread,
};
