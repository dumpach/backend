const Sequelize = require('sequelize');
const db = require('../connection');

const Migration = db.define(
  'migrations',
  {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    created_at: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('NOW'),
    },
  },
  { underscored: true, timestamps: false },
);

module.exports = Migration;
