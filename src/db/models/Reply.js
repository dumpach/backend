const Sequelize = require('sequelize');
const db = require('../connection');
const boards = require('../seeders/boards');

const generateModels = () => {
  const replyModels = {};

  boards.forEach((board) => {
    const model = db.define(
      `${board.id}_replies`,
      {
        post_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        post_thread_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        reply_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        reply_thread_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      { underscored: true, timestamps: false },
    );

    replyModels[board.id] = model;
  });

  return replyModels;
};

const Reply = generateModels();

module.exports = Reply;
