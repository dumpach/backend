const connection = require('./connection');
const migrations = require('./migrations');
const models = require('./models');
const seeders = require('./seeders');

module.exports = {
  db: connection,
  migrations,
  models,
  seeders,
};
