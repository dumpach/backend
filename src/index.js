const Koa = require('koa');
global.Promise = require('bluebird');
const { logger } = require('./modules');
const { routes } = require('./components');
const { db, migrations, seeders } = require('./db');
const { app } = require('./config');
const middlewares = require('./middlewares');

const server = new Koa();

db.authenticate()
  /* eslint-disable-next-line consistent-return */
  .then(async () => {
    logger.info('database - online');

    try {
      await db.sync({ force: true });
      logger.info('database - models syncing - success');
    } catch (err) {
      logger.error('database - models syncing - failure');
      logger.error(err);
      process.exit(1);
    }

    try {
      await migrations.init();
      logger.info('database - migrations - success');
    } catch (err) {
      logger.error('database - migrations - failure');
      logger.error(err);
      process.exit(1);
    }

    try {
      await seeders.init();
      logger.info('database - seeding - success');
    } catch (err) {
      logger.error('database - seeding - failure');
      logger.error(err);
      process.exit(1);
    }

    middlewares.forEach((middleware) => server.use(middleware));
    logger.info('server - middlewares connection - success');

    routes.forEach((route) => server.use(route));
    logger.info('server - routes initialization - success');

    /* eslint-disable-next-line consistent-return */
    server.listen(app.port, (err) => {
      if (err) {
        logger.error('server - offline');
        logger.error(err);
        process.exit(1);
      }

      logger.info('server - online');
      logger.info('all systems nominal');
    });
  })
  .catch((err) => {
    logger.error('database - offline:');
    logger.error(err);
    process.exit(1);
  });

// TODO: add boards stats(separate table)
