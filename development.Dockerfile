FROM node:10

WORKDIR /usr/src/app

COPY package.json ./
COPY package-lock.json ./

# ARG CACHEBUST=1

RUN npm install

COPY . .

EXPOSE 3000
EXPOSE 9229

CMD ["npm", "run", "dev"]